# sammy
Demo serverless app to run on Voxis.

## JSON Endpoints
Make a POST request to the given endpoint with the content-type header set to "json" and the HTTP body JSON encoded.

### random-number-generator
Returns a random number.

*HTTP body payload:*
- `min` - The lower bound for the random number (defaults to 1).
- `max` - The upper bound for the random number (defaults to 100).

*Response:*
```
{ output: 7 }
```

### random-string-generator
Returns a random alphanumeric string.

*HTTP body payload:*
- `length` - The length of the random string to generate (defaults to 20 characters).

*Response:*
```
{ output: "2NNUHDWOPPGIL8LKILMU" }
```
