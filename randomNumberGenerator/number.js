'use strict';

/**
 * @file RANDOM NUMBER GENERATOR
 * @description An example utility method for generating a random number.
 */

const random = require(`../lib/random/random.js`);

module.exports = function randomNumberGenerator (context/* , methods */) {

	const min = context.payload.min || 1;
	const max = context.payload.max || 100;
	const output = random(min, max);

	return { output };

}
