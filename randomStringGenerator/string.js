'use strict';

/**
 * @file RANDOM STRING GENERATOR
 * @description An example utility method for generating a random string.
 */

const random = require(`../lib/random/random.js`);

const ALPHABET = [
	`a`, `b`, `c`, `d`, `e`, `f`, `g`, `h`, `i`, `j`, `k`, `l`, `m`, `n`, `o`, `p`, `q`, `r`, `s`, `t`,
	`u`, `v`, `w`, `x`, `y`, `z`, `0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`,
];

module.exports = async function randomStringGenerator (context, methods) {

	// Generate a random string.
	const length = context.payload.length || 20;;
	let output = ``;

	for (let stringIndex = 0; stringIndex < length; stringIndex++) {
		const letterIndex = random(0, ALPHABET.length - 1);
		output += ALPHABET[letterIndex];
	}

	// Send the output.
	const outputUpper = output.toUpperCase();
	const responsePayload = { output: outputUpper };
	await methods.sendResponse(responsePayload, 200);

	// Do some more work after sending the output.
	await methods.log.info(`The string generated was "${outputUpper}"!`);

}
